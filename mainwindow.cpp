#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTextDocumentWriter>
#include <QMessageBox>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QString path = QFileDialog::getExistingDirectory(0, "Select folder to save", "");
    QString fileName = QFileDialog::getSaveFileName(this, "Save Text", path, "*.txt ;; *.doc ;; *.docx ;; *.odt ;; *");
    if(fileName.isEmpty()) {
        return;
    }
    QTextDocumentWriter writer(fileName);
    if(writer.write(ui->textEdit->document())) {
        QMessageBox::information(this, "Success", "Your text has been saved!");
        }
        else {
        QMessageBox::critical(this, "Fail", "Sorry, some error with save.");
        }
}



void MainWindow::on_radioButton_clicked()
{
    QMessageBox::information(this, "ON", "ON");
}

void MainWindow::on_pushButton_2_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,"Open ImageFile",QDir::currentPath(),tr("JPEG (*.jpg *.jpeg);;PNG (*.png)"));
    if(!fileName.isEmpty())
    { QImage image(fileName);
    if(image.isNull())
    { QMessageBox::information(this,"Image Viewer","Error Displaying image");
    return;
    }
    QGraphicsScene* scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);
    scene->addPixmap(QPixmap::fromImage(image));
    ui->graphicsView->fitInView(scene->sceneRect(),Qt::KeepAspectRatio);
    }
}

void MainWindow::on_pushButton_3_clicked()
{
    if(ui->lineEdit_2->text().isEmpty()){
    ui->label->setText("А там ничего не написано");
    } else { ui->label->setText(ui->lineEdit_2->text());
    }
}

void MainWindow::on_pushButton_4_clicked()
{
    ui->label->setText("Установить то, что написано выше вместо этого текста")
}

void MainWindow::on_pushButton_5_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,"Open Text File",
    QDir::currentPath(), "Text files (*.txt);;All (*)");
    if(!fileName.isEmpty()){
    QFile file(fileName);
    if(QFileInfo(fileName).suffix() == "txt" & file.open(QFile::ReadOnly |
    QFile::Text)){
    QTextStream stream(&file);
    while (!stream.atEnd()){
    ui->textEdit->setText(stream.readAll());
    }
    } else { QMessageBox::critical(this, "Ошибка открытия файла", "Выбран нетекстовый файл.");
    }
    }
}


